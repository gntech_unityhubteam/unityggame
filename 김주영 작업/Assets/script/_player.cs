﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody2D))]
public class _player : MonoBehaviour
{
    public float WalkSpeed;
    public float JumpForce;
    public AnimationClip _walk, _jump;
    public Animation _Legs;
    public Transform _Blade, _GroundCast;
    public bool mirror;
    public bool jumpbool = false;       // 점프버튼 값


    private bool _canJump, _canWalk;
    private bool _isWalk, _isJump;
    private float rot, _startScale;
    private Rigidbody2D rig;
    private Vector2 _inputAxis;
    private RaycastHit2D _hit;

    void Start()
    {
        rig = gameObject.GetComponent<Rigidbody2D>();
        _startScale = transform.localScale.x;
    }

    void Update()
    {
        // 외부 스크립트
        joy joy = GameObject.Find("JoyStickBackGround").GetComponent<joy>();
        PlayerState playerstate = GameObject.Find("GamePlayer").GetComponent<PlayerState>();
        jumpbutton jumpbutton = GameObject.Find("Button").GetComponent<jumpbutton>();


        if (!joy.MoveFlag)
        {
            joy.joyX = 0;
            joy.joyY = 0;
            
            _canJump = false;
            _isJump = false;
        }

        // playerstate에서 walkspeed와 jumpforce를 입력 받음
        WalkSpeed = playerstate.Player_speed;
        JumpForce = playerstate.Player_jumpforce;

        if (_hit = Physics2D.Linecast(new Vector2(_GroundCast.position.x, _GroundCast.position.y + 0.1f), _GroundCast.position))
        {
            if (!_hit.transform.CompareTag("Player"))
            {
                _canJump = true;
                _canWalk = true;
            }
        }
        else _canJump = false;

        //_inputAxis = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        _inputAxis = new Vector2(joy.joyX, Input.GetAxisRaw("Vertical"));

        if (jumpbutton.onjump && _canJump)
        {
            _canWalk = false;
            _isJump = true;
        }
    }

    void FixedUpdate()
    {
        joy joy = GameObject.Find("JoyStickBackGround").GetComponent<joy>();

        if (joy.joyX>0)
            mirror = false;
        if (joy.joyX<0)
            mirror = true;

        if (!mirror)
        {
           
            transform.localScale = new Vector3(_startScale, _startScale, 1);
            _Blade.transform.rotation = Quaternion.AngleAxis(rot, Vector3.forward);
        }
        if (mirror)
        {
            
            transform.localScale = new Vector3(-_startScale, _startScale, 1);
            _Blade.transform.rotation = Quaternion.AngleAxis(rot, Vector3.forward);
        }

        if (_inputAxis.x != 0)

        {
            //this.transform.Translate(new Vector2(_inputAxis.x * WalkSpeed * Time.deltaTime, rig.velocity.y));
            rig.velocity = new Vector2(_inputAxis.x * WalkSpeed * Time.deltaTime, rig.velocity.y);

            if (_canWalk)
            {
                _Legs.clip = _walk;
                _Legs.Play();
            }
        }

        else
        {

            rig.velocity = new Vector2(0, rig.velocity.y);
        }

        if (_isJump)
        {
            rig.AddForce(new Vector2(0, JumpForce));
            _Legs.clip = _jump;
            _Legs.Play();
            _canJump = false;
            _isJump = false;
        }
    }

    public bool IsMirror()
    {
        return mirror;
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, _GroundCast.position);
    }
}
