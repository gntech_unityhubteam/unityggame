﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class testmove : MonoBehaviour
{

    // 공개

    public Transform spri;
    public AnimationClip _pwalk, _pjump;
    public Animation _pLegs;
    public Transform _pBlade, _pGroundCast;
    public Camera cam;

    public int Ca_speed;        // test_speed를 받을 변수
    public int Ca_Jump;

    public float xMove = 0, yMove = 0;
    public float JoysVectorX;
    public float JoysVectorY;
    public float jumpCCopy;     // jump카운트를 받을 임시 변수
    public float jumpOcopy;     // 점프 카운트
    public int jumpCount;
    

    public bool isGrounded = false;
    public bool isJumping = false;
    public bool isMove = false;
    public bool mirror = false;





    //비공개
    private Rigidbody2D rigid;
    private RaycastHit2D _hit;
    private RaycastHit hit;

    private bool _canJump, _canWalk;
    private bool juc;       // 점프 토글
    private bool grounded = false;

    Vector3 movement;


    void Start()
    {
        rigid = gameObject.GetComponent<Rigidbody2D>();
        jumpOcopy = 0;


    }

    // Update is called once per frame
    void Update()
    {
        

        if(juc)
        {
            _canJump = true;
            _canWalk = true;
        }
        else _canJump = false;

        testPlayer_state testps = GameObject.Find("PlayerMaOB").GetComponent<testPlayer_state>();
        Ca_speed = testps.test_speed;
        Ca_Jump = testps.test_jumpforce;
        jumpCCopy = testps.jumpEc;

        JoyStick testjoy = GameObject.Find("JoyStickBackGround").GetComponent<JoyStick>();
        isMove = testjoy.testismove;

        JumpKey jumpcc = GameObject.Find("JUButton").GetComponent<JumpKey>();
        juc = jumpcc.jumpc;

        if (juc)
        {
            isJumping = true;
        }




    }

    private void FixedUpdate()
    {
        Move();         // 이동 함수
        NJUMP();        // 점프 함수

        if (isMove)
        {
            _pLegs.clip = _pwalk;
            _pLegs.Play();
        }

        if (isJumping)
        {

            _pLegs.clip = _pjump;
            _pLegs.Play();
            _canJump = false;
            isJumping = false;
        }

        //Jump();
    }

   

    void Move()
    {
        if (!isMove)
            return;

        JoyStick testjoy = GameObject.Find("JoyStickBackGround").GetComponent<JoyStick>();
        JoysVectorX = testjoy.joyvvaluex;
        JoysVectorY = testjoy.joyvvaluey;



        if (JoysVectorX > 0)
        {
            xMove = Ca_speed * Time.deltaTime;
            spri.transform.localScale = new Vector3(1, 1, 1); // right
        }

        else if (JoysVectorX < 0)
        {
            xMove = -Ca_speed * Time.deltaTime;
            spri.transform.localScale = new Vector3(-1, 1, 1); // left
        }

        else if (JoysVectorY > 0) yMove = Ca_speed * Time.deltaTime;
        else if (JoysVectorY > 0) yMove = -Ca_speed * Time.deltaTime;

        this.transform.Translate(new Vector2(xMove, yMove));

    }

    void NJUMP()
    {
        if (juc)
        {
            if (_canJump)
            {
                if (!isJumping)
                    return;

                rigid.velocity = Vector2.zero;

                Vector2 jumpvelocity = new Vector2(0, Ca_Jump);
                rigid.AddForce(jumpvelocity, ForceMode2D.Impulse);

                jumpOcopy--;

                JumpKey jumpcc = GameObject.Find("JUButton").GetComponent<JumpKey>();
                jumpcc.jumpc = false;

            }
        }
    }

    
        
    

    /*void Jump()
    {

        if (juc)
        { 
            rigid.velocity = Vector2.zero;

            Vector2 jumpvelocity = new Vector2(0, Ca_Jump);
            rigid.AddForce(jumpvelocity, ForceMode2D.Impulse);


            JumpKey jumpcc = GameObject.Find("JUButton").GetComponent<JumpKey>();
            jumpcc.jumpc = false;

        }
        
    }*/

}


