﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class testPlayer_state : MonoBehaviour
{
    public int MHP = 100;
    public int MMP = 50;
    public int health = 4;
    public int str = 4;
    public int dex = 4;
    public int intgel = 4;
    public int moveforce = 4;
    public int test_speed;  // 이동 스크립트와 연결
    public int jumpforce = 30;
    public int test_jumpforce;  // 이동 스크립트와 연결
    public int jumpEc = 2;      // 점프 가능 횟수

    private int NHP;    // 현재 HP
    private int hpz;    // hp 젠율
    private int s_hpp;  // +- 보정 hp
    private int p_hpp;  // %* 보정 hp
    private int NMP;    // 현재 mp
    private int mpz;    // 마나 젠
    private int s_mpp; // +- 보정 mp
    private int p_mpp;  // */보정 mp

    

    // Start is called before the first frame update
    void Start()
    {
        
        
    }

    // Update is called once per frame
    void Update()
    {
        test_speed = dex + moveforce;
        test_jumpforce = dex / 4 + jumpforce;
    }
    
}
