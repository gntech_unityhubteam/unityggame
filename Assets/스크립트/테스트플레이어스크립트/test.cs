﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class test : MonoBehaviour
{
    public int jumpCount;
    public int jumpforce = 3;

    public bool isGround;

    private Rigidbody2D rb;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        
        jumpCount = 1; //점프 가능횟수

        isGround = true; //땅에 있을때
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void FixedUpdate()

    {

        if (isGround)

        {

            jumpCount = 1;

            if (Input.GetKeyDown("space")) //점프

            {

                if (jumpCount == 1)
                {
                    rb.velocity = Vector2.zero;
                    Vector2 jumpvelocity = new Vector2(0, jumpforce);
                    rb.AddForce(jumpvelocity, ForceMode2D.Impulse);

                    isGround = false;

                    jumpCount = 0;

                }

            }

        }

    }

    private void OnCollisionEnter(Collision col)

    {

        if (col.gameObject.tag == "Ground")

        {

            isGround = true;    //Ground에 닿으면 isGround는 true

            jumpCount = 1;          //Ground에 닿으면 점프횟수가 1로 초기화됨

        }

    }
}
